<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '123456');

/** MySQL hostname */
define('DB_HOST', '192.168.10.50');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '[Q#;.j`>q#neRp][&:4uLDNmc%d!Gg @YNAFmb0AZ[q;}VN>EcBD+DFqIL;tW#!z');
define('SECURE_AUTH_KEY',  'Gl@:n$X,a^{2%E7/Cw2W%.V(0>>,1`I5lG1CLC0|0?t6 I v[V?.F7]=3_%#]G[1');
define('LOGGED_IN_KEY',    '3Y QwA>N4-hj_T!E5,[TU$>35^:b*miG)i#S6i&xl%7;:|^<:St/#sXR!=WD[D0(');
define('NONCE_KEY',        'S^DxfD[2%D&/C4Qh%bE97*N?S9#{IN3y!>c@3MexwI&LN#I{kxG5awS54&zE5/>T');
define('AUTH_SALT',        '1?LpJuXyW40;qVoA/V_,n5O,S-jo@Gv3F[aE+GO2<Bt5<3J;9QWy^vWV`!tmOrr4');
define('SECURE_AUTH_SALT', '8jfUZ3cZl;%2up5h]q$NgvQLmBtK /I5ADAwL%e:4|_A7~fO:n&C:PManbAjkw#q');
define('LOGGED_IN_SALT',   'st3GB*tRY:f|Ogs2itAgvI8;91_l?32S~T7zDnF(23{bIUS.ah{NIbtq)4/Ev3$y');
define('NONCE_SALT',       'RKw`}>l%w@FF1G& d74Jv]x8nX/-mi%)R;YukhlP$rE5>eW8(W4mz9OK3evr1[,E');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
